import React from "react"

export default function Header() {
  return <header className="bg-primary text-white">
    <div className="container text-center">
      <img width="125" height="125" src="/images/cryptocurrency-accepted.jpg" alt="..." className="rounded-circle" />
      <h1>КРИПТОВАЛЮТЫ</h1>
      <p className="lead"><i className="fa fa-heart text-danger" />{`{ PHP, JS}`}</p>
    </div>
  </header>
}


Header.displayName = 'HomePageHeader'

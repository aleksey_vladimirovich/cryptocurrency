<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>


## Разворачивание
Убедитесь, что  [docker](https://docs.docker.com/install/) и [docker-compose](https://docs.docker.com/compose/install/) установлены на вашей машине.

1. `git clone`
2. `create a .env file copy content from .env.docker and do not make any change`

запустите следущую команду в терминале
```
docker-compose up -d
```

when docker will finish building the containers, access the "laravel-react-app" container using following command

`docker exec -it laravel_react_app sh`

now you will be inside container

run following commands
1. `composer install && composer update`
2. `php artisan cron:refresh-database`
3. `php artisan key:gen`
4. `npm install && npm run dev`

open browser and check the following address

`http://localhost:8100`

TODO:

- [x] Add Redux
- [x] Add Passport for authentication
- [x] User Login
- [x] User Register
- [x] Users Crud
- [x] Articles Crud
- [x] Form validation Client and Server
- [ ] Reset Password
- [x] Tests
- [x] Upgrade to Laravel 7
- [x] Upgrade to React 16.13
- [x] docker



